package gr.demokritos.workreports.data.services;

import gr.demokritos.workreports.data.entities.Role;
import gr.demokritos.workreports.data.repositories.RoleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.vaadin.artur.helpers.CrudService;

import java.util.List;

@Service
public class RoleService extends CrudService<Role, Integer> {

    private static final Logger logger = LoggerFactory.getLogger(RoleService.class);

    private final RoleRepository repository;

    public RoleService(@Autowired RoleRepository repository) {
        this.repository = repository;
    }

    public List<Role> getAllRoles() {
        return repository.findAll();
    }

    public Role createRole(String name, String description) {
        Role role = new Role(name, description);
        return repository.save(role);
    }

    public Role updateRole(Role role) {
        return repository.save(role);
    }

    public Boolean deleteRole(String name) {
        try {
            Role role = repository.findByName(name);
            repository.delete(role);
            return true;
        } catch (Exception e) {
            logger.error("Something went wrong while delete role " + name);
            logger.error(e.getMessage());
            return false;
        }
    }

    public Role getRoleByName(String name) {
        return repository.findByName(name);
    }

    @Override
    protected RoleRepository getRepository() {
        return repository;
    }
}

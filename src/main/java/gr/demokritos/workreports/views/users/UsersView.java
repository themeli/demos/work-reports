package gr.demokritos.workreports.views.users;

import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.templatemodel.TemplateModel;
import gr.demokritos.workreports.data.entities.User;
import gr.demokritos.workreports.data.services.UserService;
import gr.demokritos.workreports.views.main.MainView;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.artur.helpers.CrudServiceDataProvider;

import java.util.Optional;

@Route(value = "users", layout = MainView.class)
@PageTitle("Users")
@JsModule("./src/views/users/users-view.js")
@Tag("users-view")
public class UsersView extends PolymerTemplate<TemplateModel> {

    // This is the Java companion file of a design
    // You can find the design file in
    // /frontend/src/views/src/views/users/users-view.js
    // The design can be easily edited by using Vaadin Designer
    // (vaadin.com/designer)

    // Grid is created here so we can pass the class to the constructor
    private Grid<User> grid = new Grid<>(User.class);

    @Id
    private TextField firstName;
    @Id
    private TextField lastName;
    @Id
    private TextField email;
    @Id
    private TextField username;
    @Id
    private PasswordField password;
    @Id
    private PasswordField confirmPassword;

    @Id
    private Button cancel;
    @Id
    private Button save;

    private Binder<User> binder;

    private User user = new User();

    private UserService userService;

    public UsersView(@Autowired UserService userService) {
        setId("users-view");
        this.userService = userService;
        grid.addColumn(User::getFirstName).setHeader("First Name");
        grid.addColumn(User::getLastName).setHeader("Last Name");
        grid.addColumn(User::getUsername).setHeader("Username");
        grid.addColumn(User::getEmail).setHeader("Email");
        grid.addColumn(user1 -> user1.getRole().getName()).setHeader("Role");
        grid.getColumns().forEach(column -> column.setAutoWidth(true));
        grid.setDataProvider(new CrudServiceDataProvider<User, Void>(userService));
        grid.addThemeVariants(GridVariant.LUMO_NO_BORDER);
        grid.setHeightFull();
        // Add to the `<slot name="grid">` defined in the template
        grid.getElement().setAttribute("slot", "grid");
        getElement().appendChild(grid.getElement());

        // when a row is selected or deselected, populate form
        grid.asSingleSelect().addValueChangeListener(event -> {
            if (event.getValue() != null) {
                User personFromBackend = userService.getUserByUsername(event.getValue().getUsername());
                // when a row is selected but the data is no longer available, refresh grid
                if (personFromBackend != null) {
                    populateForm(personFromBackend);
                } else {
                    refreshGrid();
                }
            } else {
                clearForm();
            }
        });

        // Configure Form
        binder = new Binder<>(User.class);

        // Bind fields. This where you'd define e.g. validation rules
        binder.bindInstanceFields(this);

        cancel.addClickListener(e -> {
            clearForm();
            refreshGrid();
        });

        save.addClickListener(e -> {
            try {
                if (this.user == null) {
                    this.user = new User();
                }
                String pwd = this.password.getValue();
                String confirmPwd = this.confirmPassword.getValue();
                if (!pwd.equals(confirmPwd)) {
                    Notification.show("Password and confirm password do no match");
                } else {
                    binder.writeBean(this.user);
                    userService.update(this.user);
                    clearForm();
                    refreshGrid();
                    Notification.show("Person details stored.");
                }
            } catch (ValidationException validationException) {
                Notification.show("An exception happened while trying to store the person details.");
            }
        });
    }

    private void refreshGrid() {
        grid.select(null);
        grid.getDataProvider().refreshAll();
    }

    private void clearForm() {
        populateForm(null);
    }

    private void populateForm(User value) {
        this.user = value;
        if (value == null) {
            this.confirmPassword.setValue("");
        }
        binder.readBean(this.user);
    }
}

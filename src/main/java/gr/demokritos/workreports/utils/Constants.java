package gr.demokritos.workreports.utils;

public class Constants {

    /* Tables */
    public static final String USERS = "users";
    public static final String ROLES = "roles";
    public static final String SUMMARIES = "summaries";

    /* Services */
    public static final String MODEL_MAPPER = "modelMapper";
    public static final String MAPPER_SERVICE = "mapperService";
}
